package com.example.restservicecors;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";

	private final AtomicLong counter = new AtomicLong();

	@CrossOrigin
	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(required = false, defaultValue = "World") String name,
							 @RequestParam(required = false, defaultValue = "neutral") String mood) {  // Added mood parameter
		System.out.println("==== get greeting ====");

		String moodMessage = "";
		if ("happy".equals(mood)) {
			moodMessage = " It's great to see you happy!";
		} else if ("sad".equals(mood)) {
			moodMessage = " Sorry to hear that, go swimming!";
		} else if ("angry".equals(mood)) {
			moodMessage = " Go drink some water";
		}

		return new Greeting(counter.incrementAndGet(), String.format(template, name) + moodMessage);
	}

}
