$(document).ready(function() {
    $("#getGreeting").click(function() {
        var userName = $("#userName").val();
        var userMood = $("#userMood").val();

        if (!userName) {
            userName = "World";
        }

        $.ajax({
            url: "http://localhost:8080/greeting",
            data: {
                name: userName,
                mood: userMood
            }
        }).then(function(data, status, jqxhr) {
            $('.greeting-id').html("The ID is " + data.id);
            $('.greeting-content').html("The content is " + data.content);
            console.log(jqxhr);
        }).fail(function() {
            alert("An error occurred");
        });
    });
});
